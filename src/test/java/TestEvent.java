import com.lenovo.commonevent.Event;

/**
 * Project:      CommonEvent
 * FileName:     TestEvent.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年3月16日 下午5:57:12
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */

/**
 * 类 TestEvent 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年3月16日下午5:57:12
 */
public class TestEvent extends Event {

    public TestEvent() {
        super(TestEvent.class.getSimpleName());
    }
}
