import java.util.Date;

import com.lenovo.commonevent.Event;
import com.lenovo.commonevent.IEventHandler;

/**
 * Project:      CommonEvent
 * FileName:     TestEventHandler.java
 * @Description: TODO
 * @author:      ligh4
 * @version      V1.0 
 * Createdate:   2015年3月16日 下午6:00:34
 * Copyright:    Copyright(C) 2014-2015
 * Company       Lenovo LTD.
 * All rights Reserved, Designed By Lenovo CIC.
 */

/**
 * 类 TestEventHandler 的实现描述：TODO 类实现描述
 * 
 * @author ligh4 2015年3月16日下午6:00:34
 */
public class TestEventHandler implements IEventHandler {

    /**
     * @author ligh4 2015年3月16日下午6:00:48
     */
    @Override
    public Object onEvent(Event event) {

        System.out.println("On event  " + event.getId() + " Type:" + event.getType());
        return new Date();
    }

}
